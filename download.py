import shutil
import os
import requests
from time import sleep
from txt import txt

class download:

    def file_path(target_path, title):
        path = f"{target_path}/{title}/"
        if not os.path.isdir(path):
            os.mkdir(path)
        return path

    def img(target_path, title, img_url_list):
        print('===============================Downloading===============================')
        path = download.file_path(target_path, title)
        db_img_path_dict = {}
        i = 0
        while i < len(img_url_list):
            for x in range(0, 20):
                try:
                    print(img_url_list[i])#列出download圖檔url
                    get_img = requests.get(img_url_list[i], stream=True)
                    if get_img.status_code == 200:
                        if r'.jpg' in img_url_list[i]:
                            extension = r'.jpg'
                        elif r'.jpeg' in img_url_list[i]:
                            extension = r'.jpeg'
                        elif r'.gif' in img_url_list[i]:
                            extension = r'.gif'
                        else:
                            extension = r'.png'
                        img_path = '{a}/{b}{c}'.format(a = path, b = str(i), c = extension)

                        with open(img_path, 'wb') as f:#給圖檔依序取名
                            f.write(get_img.content)
                            sleep(5)
                        
                        #check image download success
                        if os.path.isfile(img_path):
                            http_path = txt.get_http_path(img_path)
                            db_img_path_dict.setdefault(http_path, img_url_list[i])
                            break

                except Exception as err:
                    print(err)
                    sleep(25)
                    pass
            i+=1
        print('圖檔有' + str(len(img_url_list)))#列出有幾個圖檔
        http_path = txt.get_http_path(path)
        file_list = os.listdir(path)
        http_img_list = []
        for _file in file_list:
            http_img_list.append(http_path + r'/' + _file)
        print('===============================Download Finish===============================')
        return http_img_list, db_img_path_dict

    def img_n_video(target_path, title, video_n_img_url_list):
        img_n_video_list = []
        i = 0
        while i < len(video_n_img_url_list):
            #video
            if r'v.qq.com' in video_n_img_url_list[i]:
                print('=========================Video Downloading===============================')
                for x in range(0, 20):
                    try:
                        print(video_n_img_url_list[i])
                        video_dict = {}
                        path = download.file_path(target_path+ r'/video/', title)
                        cmd = f"you-get -o '{path}' -O '{title}' '{video_n_img_url_list[i]}'"
                        sleep(1)
                        print(cmd)
                        os.system(cmd)
                        video_path = path + f"/{title}.mp4"
                        if os.path.isfile(video_path):
                            print(video_path)
                            video_dict.setdefault(video_n_img_url_list[i], video_path)
                            img_n_video_list.append(video_dict)
                            break

                    except Exception as err:
                        print(err)
                        pass

            else:
                print('=========================Images Downloading===============================') 
                for x in range(0, 20):
                    try:
                        print(video_n_img_url_list[i])
                        img_dict = {}
                        path = download.file_path(target_path+ r'/news/', title)
                        get_img = requests.get(video_n_img_url_list[i], stream=True)
                        if get_img.status_code == 200:
                            if r'.jpg' in video_n_img_url_list[i]:
                                extension = r'.jpg'
                            elif r'.jpeg' in video_n_img_url_list[i]:
                                extension = r'.jpeg'
                            elif r'.gif' in video_n_img_url_list[i]:
                                extension = r'.gif'
                            else:
                                extension = r'.png'

                            img_path = '{a}/{b}{c}'.format(a = path, b = str(i), c = extension)
                            with open(img_path, 'wb') as f:
                                f.write(get_img.content)
                                sleep(5)
                            #check image download success
                            if os.path.isfile(img_path):
                                http_path = txt.get_http_path(img_path)
                                img_dict.setdefault(video_n_img_url_list[i], http_path)
                                img_n_video_list.append(img_dict)
                                break

                    except Exception as err:
                        print(err)
                        pass
            i+=1
        print('=========================Download Finish !!===============================')
        return img_n_video_list





