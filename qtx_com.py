from bs4 import BeautifulSoup
import requests
from time import sleep
from txt import txt
from info2mysql import info2mysql
from download import download
from datetime import date
from datetime import datetime
import signal
import functools
import sys
from os.path import exists
from os import makedirs

# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:
    #crawler_type is nba or cba
    # @time_out(43200)
    def main(crawler_type):
        x = 1
        while True:
            url = r'https://www.qtx.com/%s/news_%s.html' % (crawler_type, x)
            resp = requests.get(url)
            sleep(2)
            if resp.status_code == requests.codes.ok:
                print(url)
                soup = crawler.get_soup(url)
                crawler.get_txt_list(soup, crawler_type)
            x += 1
            if x == 100:
                break
            sleep(2)

    def get_soup(url):
        sleep(6)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                print('get_soup: ' + str(e))
                sleep(60)
                pass

    #crawler_type is nba or cba
    def get_txt_list(soup, crawler_type):
        if crawler_type == r'nba':
            table_name = r'basketball_news_nba'
            target_path = r'/www/wwwroot/data/images/basketball/news/nba/'

        elif crawler_type == r'cba':
            table_name = r'basketball_news_cba'
            target_path = r'/www/wwwroot/data/images/basketball/news/cba/'

        if not exists(target_path):
            makedirs(target_path)
        info2mysql.initDB('qtx_com', table_name)

        article_list = soup.select('div.ListItem')
        links = article_list[0].select('strong > a')
        for link in links:
            if r'/%s/' % crawler_type in link['href']:
                db_url_list = info2mysql.get_table_list(
                    'qtx_com', table_name, r'crawl_url')
                print('len(db_url_list): ' + str(len(db_url_list)))
                if len(db_url_list) > 0:
                    # avoid crawl and insert again
                    if not link['href'] in db_url_list:
                        try:
                            crawler.get_txt_content(
                                link['href'], crawler_type, table_name, target_path)
                            db_url_list.append(link['href'])
                        except Exception as err:
                            print(err)
                            pass
                    else:
                        print(r'Crawl Done !! Skip This: ' + link['href'])
                        # get_current_date_n_time
                        now = datetime.now()
                        current_time = now.strftime("%H:%M:%S")
                        today = date.today()
                        now_date = today.strftime("%Y-%m-%d")
                        current_data_n_time = f"{now_date} {current_time}"
                        print(current_data_n_time)
                        sleep(0.5)

                else:
                    try:
                        crawler.get_txt_content(
                            link['href'], crawler_type, table_name, target_path)
                        db_url_list.append(link['href'])
                    except Exception as err:
                        print(err)
                        pass

    @time_out(14400)
    def get_txt_content(page_url, crawler_type, table_name, target_path):
        # print(page_url)
        id_list = info2mysql.get_table_list('qtx_com', table_name, r'id')
        if len(id_list) > 0:
            num_list = []
            for id in id_list:
                num_list.append(int(id))
            id = int(max(num_list)) + 1
        else:
            id = 1

        soup = crawler.get_soup(page_url)
        # post_date
        if len(soup.select('div.mes')) > 0:
            post_date = soup.select('div.mes')
            post_date = txt.remove_date(post_date[0].text)
            # print(post_date)
        else:
            post_date = r'2020-01-01'

        # title
        title = soup.select('h1')
        if 'wiki' in table_name:
            title = title[0].text
        else:
            title = title[0].text
            title = title[1:]
        # print(title)

        try:
            tags = soup.select('div.h_label_box')
            tags = tags[0].select('a')
            print('---------------tags start-------------------')
            tags_list = []
            x = 1
            while x < len(tags):
                print(tags[x].text)
                tags_list.append(tags[x].text)
                x += 1
            print('---------------tags end-------------------')
        except Exception as err:
            print('tags error: ' + str(err))
            tags_list = r''
            pass

        # imgs
        content = soup.select('#artContent')
        imgs = content[0].select('img')
        img_n_video_url_list = []
        for img in imgs:
            if not r'http' in img['src']:
                if not r'/style/' in img['src']:
                    img_n_video_url_list.append(img['src'])
            else:
                img_n_video_url_list.append(img['src'])

        http_img_list, db_img_path_dict = download.img(
            target_path, title, img_n_video_url_list)

        content = soup.select('#artContent')
        content_list = str(content[0]).split('<img')
        _content = ""
        i = 0
        while i < len(content_list):
            if i == 0:
                content = txt.remove_html_tag(content_list[i])
                content = content.strip()
                content = content + '\n'
                _content += content
            else:
                content = txt.remove_html_tag(r'<' + content_list[i])
                content = content.strip()
                content = content + '\n'
                _content += content
            i += 1

        if len(db_img_path_dict) > 0:
            http_imgs_list = list(db_img_path_dict.keys())
            _content = txt.txt_content_change_img_url(
                db_img_path_dict, http_imgs_list, _content)
        else:
            http_imgs_list = []

        # print(_content)basketball_news_cba

        info2mysql.crawler2insert('qtx_com', table_name, id, post_date, title, str(
            tags_list), http_imgs_list, _content, crawler_type, page_url)


class basketball_wiki:
    # @time_out(43200)
    def get_all_items():
        soup = crawler.get_soup(r'https://www.qtx.com/encyclopedias')
        items = soup.select('div#basketball_text.basketball_text')
        links = items[0].select('a')
        for link in links:
            title = link.text
            print(title)
            print(link['href'])
            basketball_wiki.get_item_content(link['href'], r'basketball_wiki')

    def get_item_content(url, crawler_type):
        soup = crawler.get_soup(url)
        contents = soup.select('div.btw_centet')
        define = soup.select('div.definition')
        print(define[0].text)

        table_name = r'basketball_wiki'
        target_path = r'/www/wwwroot/data/images/basketball/wiki/'
        if not exists(target_path):
            makedirs(target_path)
        info2mysql.initDB('qtx_com', table_name)

        db_url_list = info2mysql.get_table_list(
            'qtx_com', table_name, r'crawl_url')
        if len(db_url_list) > 0:
            # avoid crawl and insert again
            if not url in db_url_list:
                try:
                    crawler.get_txt_content(
                        url, crawler_type, table_name, target_path)
                    db_url_list.append(url)
                except Exception as err:
                    print(err)
                    pass
            else:
                print(r'Crawl Done !! Skip This: ' + url)
                # get_current_date_n_time
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                today = date.today()
                now_date = today.strftime("%Y-%m-%d")
                current_data_n_time = f"{now_date} {current_time}"
                print(current_data_n_time)
                sleep(0.5)
        else:
            try:
                crawler.get_txt_content(
                    url, crawler_type, table_name, target_path)
                db_url_list.append(url)
            except Exception as err:
                print(err)
                pass


class football_wiki:
    # @time_out(43200)
    def get_all_items():
        soup = crawler.get_soup(r'https://www.qtx.com/encyclopedias')
        items = soup.select('div#football_text.football_text')
        links = items[0].select('a')
        for link in links:
            title = link.text
            print(title)
            print(link['href'])
            football_wiki.get_item_content(link['href'], r'football_wiki')

    def get_item_content(url, crawler_type):
        soup = crawler.get_soup(url)
        contents = soup.select('div.btw_centet')
        define = soup.select('div.definition')
        print(define[0].text)

        table_name = r'football_wiki'
        target_path = r'/www/wwwroot/data/images/football/wiki/'
        if not exists(target_path):
            makedirs(target_path)
        info2mysql.initDB('qtx_com', table_name)

        db_url_list = info2mysql.get_table_list(
            'qtx_com', table_name, r'crawl_url')
        if len(db_url_list) > 0:
            # avoid crawl and insert again
            if not url in db_url_list:
                try:
                    crawler.get_txt_content(
                        url, crawler_type, table_name, target_path)
                    db_url_list.append(url)
                except Exception as err:
                    print(err)
                    pass
            else:
                print(r'Crawl Done !! Skip This: ' + url)
                # get_current_date_n_time
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                today = date.today()
                now_date = today.strftime("%Y-%m-%d")
                current_data_n_time = f"{now_date} {current_time}"
                print(current_data_n_time)
                sleep(0.5)
        else:
            try:
                crawler.get_txt_content(
                    url, crawler_type, table_name, target_path)
                db_url_list.append(url)
            except Exception as err:
                print(err)
                pass


if __name__ == '__main__':
    while True:
        try:
            input_list = sys.argv
            if input_list[1] == r'nba':
                crawler.main(r'nba')
            elif input_list[1] == r'cba':
                crawler.main(r'cba')
            elif input_list[1] == r'basketball_wiki':
                basketball_wiki.get_all_items()
            elif input_list[1] == r'football_wiki':
                football_wiki.get_all_items()

        except Exception as err:
            print(err)
            pass
